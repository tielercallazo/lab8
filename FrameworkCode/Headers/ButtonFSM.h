/****************************************************************************

  Header file for template Flat Sate Machine
  based on the Gen2 Events and Services Framework

 ****************************************************************************/

#ifndef LastButtonFSM_H
#define LastButtonFSM_H

// Event Definitions
#include "ES_Configure.h" /* gets us event definitions */
#include "ES_Types.h"     /* gets bool type for returns */

// typedefs for the states
// State definitions for use with the query function
typedef enum
{
  Debouncing, Ready2Sample
} ButtonState_t;

// Public Function Prototypes

bool InitButtonFSM(uint8_t Priority);
bool PostButtonFSM(ES_Event_t ThisEvent);
ES_Event_t RunButtonFSM(ES_Event_t ThisEvent);
ButtonState_t QueryButtonFSM(void);
bool CheckButtonEvents(void);


#endif /* FSMTemplate_H */

