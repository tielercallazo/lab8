/****************************************************************************

  Header file for template service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef ServEncoderService_H
#define ServEncoderService_H

#include "ES_Types.h"

// Public Function Prototypes

bool InitEncoderService(uint8_t Priority);
bool PostEncoderService(ES_Event_t ThisEvent);
ES_Event_t RunEncoderService(ES_Event_t ThisEvent);
uint32_t calcRPM(void);
uint32_t GetTimeoutCount(void);
void PeriodIntResponse(void);
void InputCaptureResponse(void);
void PID(void);
int32_t returnRPMError(void);
int32_t returnRequestedDC(void);

#endif /* ServTemplate_H */

