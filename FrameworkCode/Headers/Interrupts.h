/****************************************************************************

  Header file for template service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef Interrupts_H
#define Interrupts_H

#include "ES_Types.h"
#include "ES_Events.h"

// Public Function Prototypes

bool InitInterruptsService(uint8_t Priority);
bool PostInterruptsService(ES_Event_t ThisEvent);
ES_Event_t RunInterruptsService(ES_Event_t ThisEvent);

#endif /* ServTemplate_H */

