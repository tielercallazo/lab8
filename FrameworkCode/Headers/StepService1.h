/****************************************************************************

  Header file for template service
  based on the Gen 2 Events and Services Framework

 ****************************************************************************/

#ifndef StepService1_H
#define StepService1_H

#include "ES_Types.h"
#include "ES_Events.h"

// Public Function Prototypes

bool InitStepService1(uint8_t Priority);
bool PostStepService1(ES_Event_t ThisEvent);
ES_Event_t RunStepService1(ES_Event_t ThisEvent);

#endif /* ServTemplate_H */

