/****************************************************************************
 Module
   TemplateService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ADService.h"
#include "ADMulti.h"
#include "PINDEFS.h"
#include "DCMotorService.h"
#include "EncoderService.h"

/*----------------------------- Module Defines ----------------------------*/
#define ADTimerValue 100


/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
bool CheckPotReading(void);
int16_t readAnalogInput(void);
uint32_t returnTargetRPM(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static uint16_t currentPot;
static double maxRPM = 50; // max duty cycle
static double minRPM = 0; // min duty cycle
static uint16_t targetRPM;
static uint32_t targetDC ;
uint32_t returnTargetRPM(void);
uint32_t returnTargetDC(void);

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTemplateService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitADService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************/
  // init the timers
  ES_Timer_InitTimer(ADTimer, ADTimerValue); // this timer posts the pot value
  
  // init one analog input pin
  ADC_MultiInit(1); // PE0 is the first analog input pin
  
  /*******************************************/
  
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTemplateService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostADService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTemplateService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunADService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  /********************************************/
   //in here you write your service code
  switch(ThisEvent.EventType)
  {
    case ES_INIT:
    {
      // do nothing in this case
    }
    break;
    
    case ES_TIMEOUT:
    {
      if(ThisEvent.EventParam == ADTimer) {
        // save the current analog read value
        currentPot = readAnalogInput();
        
        // scale the analog input value to be between 0 and 100 for the duty cycle
        // float slope = (maxRPM - minRPM) / 4095.0f;
        // targetRPM = (uint16_t)(slope * (double)currentPot + minRPM);
        targetRPM = (currentPot * (maxRPM - minRPM)) / 4095;
        // create an event to post to the dc motor service updating the speed
        // and post to the encoder service to print out the most current period
        ES_Event_t Event2Post;
        Event2Post.EventType = ES_SPEED_UPDATE;
        PostDCMotorService(Event2Post);
        
        // restart the ADtimer
        ES_Timer_InitTimer(ADTimer, ADTimerValue);
      }
    }
    break;
  }
  /*******************************************/
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

int16_t readAnalogInput(void) {
  uint32_t currentAnalogVal[1];
  ADC_MultiRead(currentAnalogVal);
  return currentAnalogVal[0];
}

uint32_t returnTargetRPM(void) {
  return targetRPM;
}

/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

