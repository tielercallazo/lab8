/****************************************************************************
 Module
   TemplateService.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "EncoderService.h"
#include "DCMotorService.h"

#include "PINDEFS.h"
#include "ADService.h"
#include "PWM16Tiva.h"

// the common headers for C99 types 
#include <stdint.h>
#include <stdbool.h>
#include <stdio.h>
#include <float.h>

// the headers to access the GPIO subsystem
#include "inc/hw_memmap.h"
#include "inc/hw_types.h"
#include "inc/hw_gpio.h"
#include "inc/hw_sysctl.h"
#include "inc/hw_pwm.h"
#include "driverlib/pwm.h"
#include "inc/hw_nvic.h"
#include "inc/hw_timer.h"
#include "inc/hw_sysctl.h"

// the headers to access the TivaWare Library
#include "driverlib/sysctl.h"
#include "driverlib/pin_map.h"
#include "driverlib/gpio.h"
#include "driverlib/timer.h"
#include "driverlib/interrupt.h"

/*----------------------------- Module Defines ----------------------------*/
#define EncoderTimerValue 100
#define TicksPerSec 40000000
#define TicksPerRot 512
#define TicksPerMS 40000


/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
void static writeLEDs(uint32_t Period);
void InitInputCapturePeriod(void);
void PeriodIntResponse (void);
uint32_t calcRPM(void);
uint32_t GetTimeoutCount(void);
void PID(void);

/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;

static uint32_t Period;
static uint32_t LastCapture;
static uint32_t TimeoutCount;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTemplateService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitEncoderService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************/
  // in here you write your initialization code
  
  // init port B which controls all the LEDs
  HWREG(SYSCTL_RCGCGPIO) |= (ACTIVATE_PORT_B | ACTIVATE_PORT_E);
  while ((HWREG(TOGGLE_PORT) & (ACTIVATE_PORT_B | ACTIVATE_PORT_E)) != (ACTIVATE_PORT_B
      | ACTIVATE_PORT_E)) {
  }
  printf("\r\nActivated B and E Ports.\r\n");
  
  // activate port b pins 0 through 5 for leds
  HWREG(GPIO_PORTB_BASE + GPIO_O_DEN) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI | 
    BIT4HI | BIT5HI | BIT6HI | BIT7HI);
  
  // activate port e pins 1 and 2 for leds, and PE3 for the test line
  // to calculate time to switch I/O line
  HWREG(GPIO_PORTE_BASE + GPIO_O_DEN) |= (BIT1HI | BIT2HI | BIT3HI);
  
  // activate port
  
  // set the direction of the pins on port B. High is output, low is input 
  HWREG(GPIO_PORTB_BASE + GPIO_O_DIR) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI | 
    BIT4HI | BIT5HI | BIT6HI | BIT7HI);
  
  // set the direction of the pins on port E. High is output, low is input
  HWREG(GPIO_PORTE_BASE + GPIO_O_DIR) |= (BIT1HI | BIT2HI | BIT3HI);
  
  // write the LEDs low initially
  HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) &= (BIT0LO & BIT1LO & BIT2LO & BIT3LO & 
    BIT4LO & BIT5LO);
  
  HWREG(GPIO_PORTE_BASE+(GPIO_O_DATA + ALL_BITS)) &= (BIT1LO & BIT2LO & BIT3LO);
  
  // enable input capture
  InitInputCapturePeriod();
  printf("\r\nInput Capture Setup");
  
  // start the encoder refresh timer
  ES_Timer_InitTimer(EncoderTimer, EncoderTimerValue);
  
  /*******************************************/
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTemplateService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostEncoderService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}


/****************************************************************************
 Function
    RunTemplateService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunEncoderService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  /********************************************/
  // in here you write your service code
  switch(ThisEvent.EventType)
  {
    case ES_INIT:
    {
      // init code
    }
    break;
    
    case ES_TIMEOUT:
    {
      if(ThisEvent.EventParam == EncoderTimer) {
        // query the period every 100ms / at rate of 10Hz
        // printf("\r\nEnc P:\t%d", Period);
        
        // update the LEDs
        writeLEDs(Period);
        
        // calculate the speed
        calcRPM();
        
        // restart the encoder timer
        ES_Timer_InitTimer(EncoderTimer, EncoderTimerValue);
      }
    }
    break;
  }
  
  /*******************************************/
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/

void InitInputCapturePeriod( void ){
  // start by enabling the clock to the timer (Wide Timer 0)
  HWREG(SYSCTL_RCGCWTIMER) |= SYSCTL_RCGCWTIMER_R0;
  
  // enable the clock to Port C
  HWREG(SYSCTL_RCGCGPIO) |= SYSCTL_RCGCGPIO_R2;
  
  // since we added this Port C clock init, we can immediately start
  // into configuring the timer, no need for further delay
  // make sure that timer (Timer A) is disabled before configuring
  HWREG(WTIMER0_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEN;
  HWREG(WTIMER0_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TBEN;
  
  // set it up in 32bit wide (individual, not concatenated) mode
  // the constant name derives from the 16/32 bit timer, but this is a 32/64
  // bit timer so we are setting the 32bit mode
  HWREG(WTIMER0_BASE+TIMER_O_CFG) = TIMER_CFG_16_BIT;
  
  // we want to use the full 32 bit count, so initialize the Interval Load
  // register to 0xffff.ffff (its default value :-)
  HWREG(WTIMER0_BASE+TIMER_O_TAILR) = 0xffffffff;
  
  // we don't want any prescaler (it is unnecessary with a 32 bit count)
  HWREG(WTIMER0_BASE+TIMER_O_TAPR) = 0;
  
  // change the priority level
  HWREG(NVIC_PRI23) = (HWREG(NVIC_PRI23) & ~NVIC_PRI23_INTD_M) + (1 << NVIC_PRI23_INTD_S);
  
  // set up timer A in capture mode (TAMR=3, TAAMS = 0), 
  // for edge time (TACMR = 1) and up-counting (TACDIR = 1)
  HWREG(WTIMER0_BASE+TIMER_O_TAMR) =
    (HWREG(WTIMER0_BASE+TIMER_O_TAMR) & ~TIMER_TAMR_TAAMS) |
      (TIMER_TAMR_TACDIR | TIMER_TAMR_TACMR | TIMER_TAMR_TAMR_CAP);
      
  // set up timer B in periodic mode so that it repeats the time-outs
  HWREG(WTIMER0_BASE+TIMER_O_TBMR) =
    (HWREG(WTIMER0_BASE+TIMER_O_TBMR) & ~TIMER_TBMR_TBMR_M) |
      TIMER_TBMR_TBMR_PERIOD;
  
  // To set the event to rising edge, we need to modify the TAEVENT bits 
  // in GPTMCTL. Rising edge = 00, so we clear the TAEVENT bits
  HWREG(WTIMER0_BASE+TIMER_O_CTL) &= ~TIMER_CTL_TAEVENT_M;
  
  // set the timeout to 2ms
  HWREG(WTIMER0_BASE+TIMER_O_TBILR) = TicksPerMS * 2;
  
  // enable a local timeout interrupt
  HWREG(WTIMER0_BASE+TIMER_O_IMR) |= TIMER_IMR_TBTOIM;
  
  // Now Set up the port to do the capture (clock was enabled earlier)
  // start by setting the alternate function for Port C bit 4 (WT0CCP0)
  HWREG(GPIO_PORTC_BASE+GPIO_O_AFSEL) |= BIT4HI;
  
  // Then, map bit 4's alternate function to WT0CCP0
  // 7 is the mux value to select WT0CCP0, 16 to shift it over to the
  // right nibble for bit 4 (4 bits/nibble * 4 bits)
  HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) =
    (HWREG(GPIO_PORTC_BASE+GPIO_O_PCTL) & 0xfff0ffff) + (7<<16); 
  
  // Enable pin on Port C for digital I/O
  HWREG(GPIO_PORTC_BASE+GPIO_O_DEN) |= BIT4HI;
  
  // make pin 4 on Port C into an input
  HWREG(GPIO_PORTC_BASE+GPIO_O_DIR) &= BIT4LO;
  
  // back to the timer to enable a local capture interrupt
  HWREG(WTIMER0_BASE+TIMER_O_IMR) |= TIMER_IMR_CAEIM;
  
  // enable the Timer A in Wide Timer 0 interrupt in the NVIC
  // it is interrupt number 94 so apppears in EN2 at bit 30
  HWREG(NVIC_EN2) |= (BIT30HI | BIT31HI);
  
  
  // make sure interrupts are enabled globally
  __enable_irq();
  printf("\r\nEnabled ISRs");
  
  // now kick the timer off by enabling it and enabling the timer to
  // stall while stopped by the debugger
  // HWREG(WTIMER0_BASE+TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL);
  HWREG(WTIMER0_BASE+TIMER_O_CTL) |= (TIMER_CTL_TAEN | TIMER_CTL_TASTALL | 
    TIMER_CTL_TBEN | TIMER_CTL_TBSTALL);
  printf("\r\nBegin Input Capture and Periodic Timer");
}

void InputCaptureResponse( void ){
  uint32_t ThisCapture;
  
  // start by clearing the source of the interrupt, the input capture event
  HWREG(WTIMER0_BASE+TIMER_O_ICR) = TIMER_ICR_CAECINT;
  
  // now grab the captured value and calculate the period
  ThisCapture = HWREG(WTIMER0_BASE+TIMER_O_TAR);
  Period = ThisCapture - LastCapture;
  
  // update LastCapture to prepare for the next edge 
  LastCapture = ThisCapture;
}

void PeriodIntResponse (void) {
  // debug line raise
  HWREG(GPIO_PORTE_BASE+(GPIO_O_DATA + ALL_BITS)) |= (BIT3HI);
  
  // start by clearning the source of the interrupt
  HWREG(WTIMER0_BASE + TIMER_O_ICR) = TIMER_ICR_TBTOCINT;
  ++TimeoutCount;
  PID();
  ES_Event_t Event2Post;
  Event2Post.EventType = ES_PID_UPDATE;
  PostDCMotorService(Event2Post);
  // debug line lower
  HWREG(GPIO_PORTE_BASE+(GPIO_O_DATA + ALL_BITS)) &= (BIT3LO);
}

uint32_t GetTimeoutCount(void) {
  return TimeoutCount;
}

uint32_t GetPeriod( void ){
  return Period;
}

void writeLEDs (uint32_t encPeriod) {
  uint32_t minEncPeriod = 20000;
  uint32_t maxEndPeriod = 60000;
  uint32_t endInterval = (maxEndPeriod - minEncPeriod)/7;
  
  // turn off all the leds each time this is run
  HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) &= (BIT0LO & BIT1LO & BIT2LO & BIT3LO & 
    BIT4LO & BIT5LO);
  
  HWREG(GPIO_PORTE_BASE+(GPIO_O_DATA + ALL_BITS)) &= (BIT1LO & BIT2LO);
  
  // using the current period, write the LEDs to be on or off
  if (encPeriod < minEncPeriod + 1 * endInterval) {
    HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) |= (BIT0HI); 
  } else if(encPeriod < minEncPeriod + 2 * endInterval && encPeriod >= minEncPeriod + 1 * endInterval) {
    HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) |= (BIT0HI | BIT1HI); 
  } else if(encPeriod < minEncPeriod + 3 * endInterval && encPeriod >= minEncPeriod + 2 * endInterval) {
    HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) |= (BIT0HI | BIT1HI | BIT2HI); 
  } else if(encPeriod < minEncPeriod + 4 * endInterval && encPeriod >= minEncPeriod + 3 * endInterval) {
    HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI); 
  } else if(encPeriod < minEncPeriod + 5 * endInterval && encPeriod >= minEncPeriod + 4 * endInterval) {
    HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI | BIT4HI); 
  } else if(encPeriod < minEncPeriod + 6 * endInterval && encPeriod >= minEncPeriod + 5 * endInterval) {
    HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI | BIT4HI | BIT5HI);
  } else if(encPeriod < minEncPeriod + 7 * endInterval && encPeriod >= minEncPeriod + 6 * endInterval) {
    HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI | BIT4HI | BIT5HI);
    HWREG(GPIO_PORTE_BASE+(GPIO_O_DATA + ALL_BITS)) |= (BIT1HI);
  } else if(encPeriod >= minEncPeriod + 7 * endInterval) {
    HWREG(GPIO_PORTB_BASE+(GPIO_O_DATA + ALL_BITS)) |= (BIT0HI | BIT1HI | BIT2HI | BIT3HI | BIT4HI | BIT5HI);
    HWREG(GPIO_PORTE_BASE+(GPIO_O_DATA + ALL_BITS)) |= (BIT1HI | BIT2HI);
  } else {
  }
}


uint32_t calcRPM(void) {
  uint32_t RPM;
  // convert the current encoder period to RPM value
  RPM = (TicksPerSec*60) / (Period * TicksPerRot);
  RPM = (RPM*10)/59;
  return RPM;
}

int32_t CurrentRPM;
int32_t TargetRPM;
static int32_t RPMError = 0;
static int32_t RequestedDC;

int32_t returnRequestedDC(void) {
  return RequestedDC;
}

int32_t returnRPMError(void);
int32_t returnRPMError(void) {
  return RPMError;
}

static float K_P = 0.8; // 12 works for some reason
static float K_I = 0.01;
static int32_t SumError = 0;

void PID(void){
  CurrentRPM = calcRPM();
  TargetRPM = returnTargetRPM();
  
  RPMError = TargetRPM - CurrentRPM;
  SumError += RPMError; 
  RequestedDC = K_P * (RPMError + (K_I * SumError));
  
  if(RequestedDC > 100) {
    RequestedDC = 100;
    SumError -= RPMError; // anti windup
  } else if (RequestedDC < 0) {
    RequestedDC = 0;
    SumError -=RPMError; // anti windup
  }
}



/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

