/****************************************************************************
 Module
   SPI.c

 Revision
   1.0.1

 Description
   This is a template file for implementing a simple service under the
   Gen2 Events and Services Framework.

 Notes

 History
 When           Who     What/Why
 -------------- ---     --------
 01/16/12 09:58 jec      began conversion from TemplateFSM.c
****************************************************************************/
/*----------------------------- Include Files -----------------------------*/
/* include header files for this state machine as well as any machines at the
   next lower level in the hierarchy that are sub-machines to this machine
*/
#include "ES_Configure.h"
#include "ES_Framework.h"
#include "Interrupts.h"
#include "ADMulti.h"
#include "PINDEFS.h" // update this 
#include "DCMotorService.h"
#include "inc/hw_ssi.h"
#include "inc/hw_nvic.h"
#include "RobotFSM.h"

/*----------------------------- Module Defines ----------------------------*/
#define RequestCommandByte 0xAA
#define STOP 0x00
#define CW_90 0x02
#define CW_45 0x03
#define CCW_90 0x04
#define CCW_45 0x05
#define FORWARD_HALF 0x08
#define FORWARD_FULL 0x09
#define REVERSE_HALF 0x10
#define REVERSE_FULL 0x11
#define ALIGN 0x20
#define DRIVE_TAPE 0x40
#define DEFAULT 0xff
/*---------------------------- Module Functions ---------------------------*/
/* prototypes for private functions for this service.They should be functions
   relevant to the behavior of this service
*/
void InitSPI(void);
void RequestCommand(void);
void GenerateEvent (uint8_t newCommand);
/*---------------------------- Module Variables ---------------------------*/
// with the introduction of Gen2, we need a module level Priority variable
static uint8_t MyPriority;
static volatile uint32_t command = 1000;

/*------------------------------ Module Code ------------------------------*/
/****************************************************************************
 Function
     InitTemplateService

 Parameters
     uint8_t : the priorty of this service

 Returns
     bool, false if error in initialization, true otherwise

 Description
     Saves away the priority, and does any
     other required initialization for this service
 Notes

 Author
     J. Edward Carryer, 01/16/12, 10:00
****************************************************************************/
bool InitInterruptsService(uint8_t Priority)
{
  ES_Event_t ThisEvent;

  MyPriority = Priority;
  /********************************************/
  // init code for SPI
  
  /*******************************************/
  InitSPI();
  printf("SPI Initialized!\r\n");
  
  // Initialize a 100ms timer to request commands
  ES_Timer_InitTimer(CommandRequestTimer, 100);
  
  // post the initial transition event
  ThisEvent.EventType = ES_INIT;
  if (ES_PostToService(MyPriority, ThisEvent) == true)
  {
    return true;
  }
  else
  {
    return false;
  }
}

/****************************************************************************
 Function
     PostTemplateService

 Parameters
     EF_Event_t ThisEvent ,the event to post to the queue

 Returns
     bool false if the Enqueue operation failed, true otherwise

 Description
     Posts an event to this state machine's queue
 Notes

 Author
     J. Edward Carryer, 10/23/11, 19:25
****************************************************************************/
bool PostInterruptsService(ES_Event_t ThisEvent)
{
  return ES_PostToService(MyPriority, ThisEvent);
}

/****************************************************************************
 Function
    RunTemplateService

 Parameters
   ES_Event_t : the event to process

 Returns
   ES_Event, ES_NO_EVENT if no error ES_ERROR otherwise

 Description
   add your description here
 Notes

 Author
   J. Edward Carryer, 01/15/12, 15:23
****************************************************************************/
ES_Event_t RunInterruptsService(ES_Event_t ThisEvent)
{
  ES_Event_t ReturnEvent;
  ReturnEvent.EventType = ES_NO_EVENT; // assume no errors
  /********************************************/
   //in here you write your service code
  switch(ThisEvent.EventType)
  {
    case ES_INIT:
    {
      // do nothing in this case
    }
    break;
    
    case ES_TIMEOUT:
      if (ThisEvent.EventParam == CommandRequestTimer) {
        // printf("Command: %d\r\n", command);
        // Request a command
        RequestCommand();
        // printf("Reinitialize timer\r\n");
        ES_Timer_InitTimer(CommandRequestTimer, 100);
      }
    break;
  }
  /*******************************************/
  return ReturnEvent;
}

/***************************************************************************
 private functions
 ***************************************************************************/
void InitSPI(void){
  //  1. Enable the clock to the GPIO port 
  // Port A by default is SSI power on default 
  // PA2-5
  HWREG(SYSCTL_RCGCGPIO) |= (ACTIVATE_PORT_A);
  
  //  2. Enable the clock to SSI module
  HWREG(SYSCTL_RCGCSSI) |= SYSCTL_RCGCSSI_R0;
  
  //  3. Wait for the GPIO port to be ready
  while ((HWREG(TOGGLE_PORT) & ACTIVATE_PORT_A) != ACTIVATE_PORT_A) {
  }
  
  //  4. Program the GPIO to use the alternate functions on the SSI pins
  // unnecessary since this is default for PA2-5
  
  //  5. Set mux position in GPIOPCTL to select the SSI use of the pins 
  uint8_t mux_val = 2; 
  // the  value being multiplied by bits per nibble is the pin on the port A 
  HWREG(GPIO_PORTA_BASE+GPIO_O_AFSEL) |= (BIT2HI | BIT3HI | BIT4HI | BIT5HI);
  HWREG(GPIO_PORTA_BASE + GPIO_O_PCTL) = (HWREG(GPIO_PORTA_BASE + GPIO_O_PCTL) & 0xff0000ff) +
    (mux_val<<(2*BITS_PER_NYBBLE)) + (mux_val<<(3*BITS_PER_NYBBLE)) + 
      (mux_val<<(4*BITS_PER_NYBBLE)) + (mux_val<<(5*BITS_PER_NYBBLE));

  //  6. Program the port lines for digital I/O
  HWREG(GPIO_PORTA_BASE + GPIO_O_DEN) |= (BIT5HI | BIT4HI | BIT3HI | BIT2HI);
  
  //  7. Program the required data directions on the port lines
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) |= (BIT5HI | BIT3HI | BIT2HI);
  HWREG(GPIO_PORTA_BASE + GPIO_O_DIR) &= (BIT4LO);
  
  //  8. If using SPI mode 3, program the pull-up on the clock line
  // enable the PUR on PA2 which is SSI0 SCK
  HWREG(GPIO_PORTA_BASE + GPIO_O_PUR) |= BIT2HI; // enable pull up on SSI0 SCK
  
  //  9. Wait for the SSI0 to be ready
  while((HWREG(SYSCTL_RCGCSSI) & SYSCTL_RCGCSSI_R0) != SYSCTL_RCGCSSI_R0){
  }
  
  //  10. Make sure that the SSI is disabled before programming mode bits
  HWREG(SSI0_BASE + SSI_O_CR1) &= ~SSI_CR1_SSE;
  
  //  11. Select master mode (MS) & TXRIS indicating End of Transmit (EOT)
  HWREG(SSI0_BASE + SSI_O_CR1) &= ~SSI_CR1_MS;
  HWREG(SSI0_BASE + SSI_O_CR1) |= SSI_CR1_EOT;
  
  //  12. Configure the SSI clock source to the system clock
  HWREG(SSI0_BASE + SSI_O_CC) &= SSI_CC_CS_SYSPLL; 
  
  //  13. Configure the clock pre-scaler
  // target freq: 917431 HZ
  // step 1) set divisor = 2
  // step 2) solve for SCR (prescaler) and if it's in range,
  // shift it in the right location
  HWREG(SSI0_BASE + SSI_O_CPSR) = 2;
  HWREG(SSI0_BASE + SSI_O_CR0) = (HWREG(SSI0_BASE + SSI_O_CR0) & ~SSI_CR0_SCR_M) |
    (21 << SSI_CR0_SCR_S);
  
  
  //  14. Configure clock rate (SCR), phase & polarity (SPH, SPO), mode (FRF), data size (DSS)
  // for PCB generator (timing diagram)
  // Phase is 1 because writing data happens on even edges
  // Polarity is 1 because serial clock idles high
  HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_SPH; // this is phase 1
  HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_SPO; // this is polarity 1
  HWREG(SSI0_BASE + SSI_O_CR0) &= ~SSI_CR0_FRF_M; 
  HWREG(SSI0_BASE + SSI_O_CR0) |= SSI_CR0_DSS_8; 
  
  //  15. Locally enable interrupts (TXIM in SSIIM)
  HWREG(SSI0_BASE + SSI_O_IM) |= SSI_IM_TXIM;

  //  16. Make sure that the SSI is enabled for operation
  HWREG(SSI0_BASE + SSI_O_CR1) |= SSI_CR1_SSE;
  HWREG(SSI0_BASE + SSI_O_DR) = RequestCommandByte;
  //  17. Enable the NVIC interrupt for the SSI when starting to transmit
  HWREG(NVIC_EN0) = BIT7HI;  //Interrupt number 7 for SSIO
  
    // make sure interrupts are enabled globally
  __enable_irq();
}

void RequestCommand(void) {
  
  // Write request byte to the data register
  HWREG(SSI0_BASE + SSI_O_DR) = RequestCommandByte;
  // Enable interrupts
  HWREG(NVIC_EN0) = BIT7HI;
}

void CommandGeneratorInterrupt (void) {
  // Got an interrupt so disable it for now
  HWREG(NVIC_DIS0) = BIT7HI;
  
  // Read command
  uint8_t newCommand = HWREG(SSI0_BASE + SSI_O_DR);
  
  if (newCommand != command) {
    // post event
    GenerateEvent(newCommand);
    command = newCommand;
  }
}

void GenerateEvent (uint8_t newCommand) {
  ES_Event_t ThisEvent;
  
  switch (newCommand) {
    case STOP: {
      ThisEvent.EventType = ES_STOP;
      // post to robot
    }
    break;
    
    case DEFAULT: {
      ThisEvent.EventType = ES_STOP;
      // post to robot
    }
    break;
    
    case FORWARD_FULL: {
      ThisEvent.EventType = ES_FORWARD_FULL;
      // post to robot
    }
    break;
    
    case FORWARD_HALF: {
      ThisEvent.EventType = ES_FORWARD_HALF;
      // post to robot
    }
    break;
    
    case REVERSE_FULL: {
      ThisEvent.EventType = ES_BACKWARD_FULL;
      // post to robot
    }
    break;
    
    case REVERSE_HALF: {
      ThisEvent.EventType = ES_BACKWARD_HALF;
      // post to robot
    }
    break;
    
    case CW_90: {
      ThisEvent.EventType = ES_CW_90;
      // post to robot
    }
    break;
    
    case CW_45: {
      ThisEvent.EventType = ES_CW_45;
      // post to robot
    }
    break;
    
    case CCW_90: {
      ThisEvent.EventType = ES_CCW_90;
      // post to robot
    }
    break;
    
    case CCW_45: {
      ThisEvent.EventType = ES_CCW_45;
      // post to robot
    }
    break;
    
    case ALIGN: {
      ThisEvent.EventType = ES_ALIGN;
      // post to robot
      
    }
    break;
    
    case DRIVE_TAPE: {
      ThisEvent.EventType = ES_DRIVE_TIL_TAPE;
      // post to robot
    }
    break;
  }
  PostRobotFSM(ThisEvent);
}
/*------------------------------- Footnotes -------------------------------*/
/*------------------------------ End of file ------------------------------*/

